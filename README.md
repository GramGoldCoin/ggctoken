GGC Token Documentation
====================




function descriptions
------------
@dev Allow current contract owner transfer ownership to other address

    function AssignGGCOwner(address _ownerContract) 
    public 
    onlyOwner 
    notNull(_ownerContract);

@dev Check if the address is a wallet or a contract

    function isContract(address _addr) 
    private 
    view 
    returns (bool) 


@dev transfer _value from msg.sender to receiver
Both sender and receiver pays a transaction fees
The transaction fees will be transferred into GGCPool and GGEPool

    function transfer(address _to, uint256 _value) 
    public 
    notNull(_to) 
    returns (bool success)


@dev transfer _value from contract owner to receiver
Both contract owner and receiver pay transaction fees 
The transaction fees will be transferred into GGCPool and GGEPool

    function transferFrom(address _from, address _to, uint256 _value) 
    public 
    notNull(_to) 
    returns (bool success) 



@dev calculate transaction fee base on address and value.
Check whiteList

    function feesCal(address _addr, uint256 _value)
    public
    view
    notNull(_addr) 
    returns (uint256 _ggcFee, uint256 _ggeFee)


@dev both transfer and transferfrom are dispatched here
Check blackList

    function _transfer(address _from, address _to, uint256 _value) 
    internal 
    notNull(_from) 
    notNull(_to) 
    returns (bool) 


@dev Approve the passed address to spend the specified amount of tokens on behalf of msg.sender.

@param _spender The address which will spend the funds.

@param _value The amount of tokens to be spent.

    function approve(address _spender, uint256 _value) 
    public 
    returns (bool success) 



@dev Function to check the amount of tokens that an owner allowed to a spender.

@param _tokenOwner address The address which owns the funds.

@param _spender address The address which will spend the funds.

@return A uint256 specifying the amount of tokens still available for the spender.


    function allowance(address _tokenOwner, address _spender) 
    public 
    view 
    returns (uint256 remaining) 


@dev Reject all ERC223 compatible tokens

@param from_ address The address that is transferring the tokens

@param value_ uint256 the amount of the specified token

@param data_ Bytes The data passed from the caller.

    function tokenFallback(address from_, uint256 value_, bytes data_) 
    external 

